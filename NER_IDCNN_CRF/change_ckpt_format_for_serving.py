import time
import os
import tensorflow as tf

trained_checkpoint_prefix = 'ckpt/ner.ckpt'
export_dir = os.path.join('saved_model', time.strftime("%Y%m%d-%H%M%S"))

loaded_graph = tf.Graph()
with tf.compat.v1.Session(graph=loaded_graph) as sess:
    # Restore from checkpoint
    loader = tf.compat.v1.train.import_meta_graph(trained_checkpoint_prefix + '.meta')
    loader.restore(sess, trained_checkpoint_prefix)

    # Export checkpoint to SavedModel
    builder = tf.compat.v1.saved_model.builder.SavedModelBuilder(export_dir)
    builder.add_meta_graph_and_variables(sess, [tf.saved_model.TRAINING], strip_default_attrs=True)

builder.add_meta_graph([tf.saved_model.SERVING], strip_default_attrs=True)
builder.save()